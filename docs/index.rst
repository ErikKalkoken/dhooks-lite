.. dhooks-lite documentation master file, created by
   sphinx-quickstart on Mon Nov 11 23:39:47 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.md
   :parser: myst_parser.sphinx_

.. toctree::
   :hidden:
   :maxdepth: 3

   api
